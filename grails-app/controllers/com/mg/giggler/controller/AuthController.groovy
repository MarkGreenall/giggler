package com.mg.giggler.controller

// Import the domain classes
import com.mg.giggler.domain.Property
import com.mg.giggler.domain.PropertyUsers
import com.mg.giggler.domain.Users
import com.mg.giggler.domain.AuthLevels

// Import the needed converters
import grails.converters.JSON
import grails.converters.XML

// Auth Class

class AuthController {


    // Action for Pre-Auth a user.

    def doPreAuth() {

        // Get the input data
        def userUsername    =   request.getParameter('userUsername')
        def userPassword    =   request.getParameter('userPassword')
        def settingType     =   request.getParameter('settingType')

        // Set default to not authed
        def isAuthed        =   0

        // Define some other variables
        def myList, userKey, userExpiry, userActive, actionError


        if (userUsername == null || userUsername == "" || userPassword == null || userPassword == "") {

            // One of the fields is blank so return a not authed value
            isAuthed    =   0
            actionError =   1

        } else {

            // Get the user details from the DB
            def myBCrud = Users.createCriteria().list(offset: 0, max: 1, sort: 'userKey', order: 'desc', cache: true) {
                eq('userUsername', userUsername.toString())
            }

            // Check to see if input password matches the DB stored password.
            if (myBCrud && userPassword.equals(myBCrud[0].userPassword.toString())) {

                // User is authed so return their unique user key
                isAuthed    =   1

                userKey     =   myBCrud[0].userKey
                userExpiry  =   myBCrud[0].userExpiry
                userActive  =   myBCrud[0].userActive
                userUsername=   myBCrud[0].userUsername

            } else {

                // Not authed so set to not authed.
                isAuthed    =   0
                actionError =   2

            }

        }


        // Check to see if user is authed, then return values accordingly.

        if(isAuthed == 1) {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), userKey: userKey.toString(), userExpiry: userExpiry.toString(), userActive: userActive.toString()]

        } else {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), actionReason: actionError.toString()]

        }

        // Determine the type requested and return in requested format.

        if(settingType.toString() == "JSON") {

            // Render to JSON
            render(myList as JSON)

        } else if(settingType.toString() == "XML") {

            // Render to XML
            render(myList as XML)

        } else {

            // No type specified so render as plain text
            render(myList.toString())

        }

    }








    // Get property details from propertyKey

    def getPropertyDetails() {

        // Get the propertyKey from input
        def userKey         =   request.getParameter('userKey')
        def propertyKey     =   request.getParameter('propertyKey')
        def settingType     =   request.getParameter('settingType')

        // Define some more variables
        def propertyActive, authExpiry, isAuthed, propertyTitle, propertyComment, myList, userAuthLevel, minAuthLevel, actionError


        if (userKey == null || userKey == "" || propertyKey == null || propertyKey == "") {

            isAuthed    =   0
            actionError =   1

        } else {

            // Get the property details
            def myBCrud = Property.createCriteria().list(offset: 0, max: 1, sort: 'propertyKey', order: 'desc', cache: true) {
                eq('propertyKey', propertyKey.toString())
                and {

                    eq('propertyActive', 1)

                }
            }


            // Now get the propertyAuth details

            def myCCrud = PropertyUsers.createCriteria().list(offset: 0, max: 1, sort: 'propertyKey', order: 'desc', cache: true) {
                eq('propertyKey', propertyKey.toString())
                and {

                    eq('userKey', userKey.toString())

                }
            }


            // Check to see that the user key is valid and not expired

            if (myBCrud && myBCrud[0] && myCCrud && myCCrud[0]) {


                // Get the variables from DB values

                propertyActive  =   myBCrud[0].propertyActive
                propertyKey     =   myBCrud[0].propertyKey
                propertyTitle   =   myBCrud[0].propertyTitle
                propertyComment =   myBCrud[0].propertyComment
                authExpiry      =   myCCrud[0].authExpiry

                // Get the user's auth level, and the property min auth level.

                userAuthLevel   =   myCCrud[0].authLevel
                minAuthLevel    =   myBCrud[0].propertyAuthLevel


                // If user's auth level for this property is below
                // the minimum needed then not authed.

                if(userAuthLevel.toInteger() >= minAuthLevel.toInteger()) {

                    isAuthed    =   1

                } else {

                    // User is not authed.

                    isAuthed    =   0
                    actionError =   2

                }


            } else {

                // User is not authed.

                isAuthed    =   0
                actionError =   3

            }

            // End of authed

        }




        // If not authed then output no auth code, else return details
        // Check to see if user is authed, then return values accordingly.

        if(isAuthed == 1) {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), authExpiry: authExpiry.toString(), propertyActive: propertyActive.toString(), propertyKey: propertyKey.toString(), propertyTitle: propertyTitle.toString(), propertyComment: propertyComment.toString()]

        } else {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), actionError: actionError.toString()]

        }



        // Determine the type requested and return in requested format.

        if(settingType.toString() == "JSON") {

            // Render to JSON
            render(myList as JSON)

        } else if(settingType.toString() == "XML") {

            // Render to XML
            render(myList as XML)

        } else {

            // No type specified so render as plain text
            render(myList.toString())

        }

    }







    def getAuthLevels() {


        // Get the propertyKey from input
        def authKey         =   request.getParameter('authKey')
        def settingType     =   request.getParameter('settingType')

        // Define some more variables
        def propertyActive, authExpiry, isAuthed, propertyTitle, propertyComment, myList, userAuthLevel, actionError
        def myBCrud, myCCrud


        if (authKey == null || authKey == "") {

            isAuthed    =   0
            actionError =   1

        } else {



            // Now get the propertyAuth details

            myCCrud = PropertyUsers.createCriteria().list(offset: 0, max: 1, sort: 'propertyKey', order: 'desc', cache: true) {
                eq('propertyKey', "1")
                and {

                    eq('userKey', authKey.toString())

                }
            }


            // Get the auth levels

            myBCrud = AuthLevels.createCriteria().list(offset: 0, max: 100, sort: 'authId', order: 'desc', cache: true) {
                isNotNull('authId')
            }



            // Check to see that the user key is valid and not expired

            if (myBCrud && myBCrud[0] && myCCrud && myCCrud[0]) {


                // Get the variables from DB values
                userAuthLevel   =   myCCrud[0].authLevel


                // If user's auth level for this property is below
                // the minimum needed then not authed.

                if(userAuthLevel.toInteger() >= 10000) {


                    isAuthed    =   1


                } else {

                    // User is not authed.

                    isAuthed    =   0
                    actionError =   2

                }


            } else {

                // User is not authed.

                isAuthed    =   0
                actionError =   3

            }

            // End of authed

        }




        // If not authed then output no auth code, else return details
        // Check to see if user is authed, then return values accordingly.

        if(isAuthed == 1) {

            // Return the auth levels as a list

            if(settingType.toString() == "JSON") {

                // Render to JSON
                render(myBCrud.toList() as JSON)

            } else if(settingType.toString() == "XML") {

                // Render to XML
                render(myBCrud.toList() as XML)

            } else {

                // No type specified so render as plain text
                render(myBCrud.toList())

            }

        } else {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), actionError: actionError.toString()]


            if(settingType.toString() == "JSON") {

                // Render to JSON
                render(myList as JSON)

            } else if(settingType.toString() == "XML") {

                // Render to XML
                render(myList as XML)

            } else {

                // No type specified so render as plain text
                render(myList.toString())

            }
        }



    }








    // Create a new user


    def createUser() {


        // Get the propertyKey from input
        def authKey         =   request.getParameter('authKey')
        def settingType     =   request.getParameter('settingType')

        // The new user's details
        def userUsername   =   request.getParameter('userUsername')
        def userPassword   =   request.getParameter('userPassword')
        def userActive     =   request.getParameter('userActive')
        def userExpiry     =   request.getParameter('userExpiry')
        def userKey        =   request.getParameter('userKey')


        // Define some more variables
        def isAuthed, actionError, userAuthLevel, myList
        def propertyUsersDomain


        if (authKey == null || authKey == "" || userKey == null || userKey == "" || userUsername == null || userUsername == "" || userPassword == null || userPassword == "" || userKey == null || userKey == "" || userActive == null || userActive == "" || userExpiry == null || userExpiry == "") {

            isAuthed    =   0
            actionError =   1

        } else {



            // Now get the propertyAuth details

            propertyUsersDomain = PropertyUsers.createCriteria().list(offset: 0, max: 1, sort: 'propertyKey', order: 'desc', cache: true) {

                eq('propertyKey', "1")

                and {

                    eq('userKey', userKey.toString())

                }

            }



            // Check to see that the user key is valid and not expired

            if(propertyUsersDomain && propertyUsersDomain[0]) {


                // Get the variables from DB values
                userAuthLevel   =   propertyUsersDomain[0].authLevel


                // If user's auth level for this property is below
                // the minimum needed then not authed.

                if(userAuthLevel.toInteger() >= 10000) {


                    // Set authed flag to true
                    isAuthed    =   1


                    // TODO Check to see if the user exists already


                    // Make a new record for this user's details
                    def userDomain = new Users(userActive: userActive.toInteger(), userExpiry: userExpiry.toString(), userKey: userKey.toString(), userUsername: userUsername.toString(), userPassword: userPassword.toString())


                    // check to see if save happened - if not then do not auth and throw error.
                    if (!userDomain.save()) {

                        isAuthed    =   0
                        actionError =   4

                    }


                } else {

                    // User is not authed.

                    isAuthed    =   0
                    actionError =   2

                }


            } else {

                // User is not authed.

                isAuthed    =   0
                actionError =   3

            }

            // End of authed

        }


        // If not authed then output no auth code, else return details
        // Check to see if user is authed, then return values accordingly.

        if(isAuthed == 1) {

            myList = [actionStatus: isAuthed.toString()]

        } else {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), actionError: actionError.toString()]

        }



        if(settingType.toString() == "JSON") {

            // Render to JSON
            render(myList as JSON)

        } else if(settingType.toString() == "XML") {

            // Render to XML
            render(myList as XML)

        } else {

            // No type specified so render as plain text
            render(myList.toString())

        }


    }








    // Creates a user authorisation against propertyKey

    def createPropertyAuth() {



    }









    // Checks to see if a user is authed for property

    def isUserAuthed() {



    }







    // Checks to see if the user is active

    def getUserLongevity() {

        def userKey         =   request.getParameter('userKey')
        def settingType     =   request.getParameter('settingType')

        // Define some more variables
        def isAuthed, actionError, myList, userDomain, myUserActive, userExpiry
        def userActive

        if (userKey == null || userKey == "") {

            isAuthed    =   0
            actionError =   1

        } else {


            // get the authorising user's details.

            userDomain = Users.createCriteria().list(offset: 0, max: 1, sort: 'userKey', order: 'desc', cache: true) {
                eq('userKey', userKey.toString())
            }


            if(userDomain && userDomain[0]) {

                isAuthed    =   1

                userActive  =   userDomain[0].userActive
                userExpiry  =   userDomain[0].userExpiry

            } else {

                isAuthed    =   0
                actionError =   2

            }


        }


        if(isAuthed == 1) {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), userActive: userActive.toString(), userExpiry: userExpiry.toString()]

        } else {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), actionError: actionError.toString()]

        }



        // Determine the type requested and return in requested format.

        if(settingType.toString() == "JSON") {

            // Render to JSON
            render(myList as JSON)

        } else if(settingType.toString() == "XML") {

            // Render to XML
            render(myList as XML)

        } else {

            // No type specified so render as plain text
            render(myList.toString())

        }


    }








    // Get the expiry epoch for user's auth key

    def getUserAuthExpiry() {



    }









    def getUserDetails() {

        // Get the user's details

        // Get the propertyKey from input
        def userKey         =   request.getParameter('userKey')
        def authKey         =   request.getParameter('authKey')
        def settingType     =   request.getParameter('settingType')

        // Define some more variables
        def isAuthed, myList, myACrud, myBCrud, myCCrud, myAuthUserActive, myAuthUserPropertyKey, actionError
        def userUsername, userPassword, userActive, userExpiry

        if (userKey == null || userKey == "" || authKey == null || authKey == "") {

            isAuthed    =   0
            actionError =   1

        } else {


            // get the authorising user's details.

            myACrud = PropertyUsers.createCriteria().list(offset: 0, max: 1, sort: 'userKey', order: 'desc', cache: true) {
                eq('userKey', authKey.toString())
            }

            myCCrud = Users.createCriteria().list(offset: 0, max: 1, sort: 'userKey', order: 'desc', cache: true) {
                eq('userKey', authKey.toString())
            }


            // Now get the user to check's details

            myBCrud = Users.createCriteria().list(offset: 0, max: 1, sort: 'userKey', order: 'desc', cache: true) {
                eq('userKey', userKey.toString())
            }



            if(myACrud && myACrud[0] && myBCrud && myBCrud[0]) {

                // Now see if the authing user is authed for main root property "1"

                myAuthUserActive        =   myCCrud[0].userActive
                myAuthUserPropertyKey   =   myACrud[0].propertyKey

                // Check that they are active, and that they have access to property "1"
                if(myAuthUserActive.toInteger() == 1 && myAuthUserPropertyKey.toInteger() == 1) {

                    // User is authed - return details.

                    isAuthed        =   1

                    userUsername    =   myBCrud[0].userUsername
                    userPassword    =   myBCrud[0].userPassword
                    userActive      =   myBCrud[0].userActive
                    userExpiry      =   myBCrud[0].userExpiry

                } else {

                    isAuthed        =   0
                    actionError     =   3

                }

            } else {

                isAuthed        =   0
                actionError     =   2

            }



        }



        // If not authed then output no auth code, else return details
        // Check to see if user is authed, then return values accordingly.

        if(isAuthed == 1) {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), userKey:  userKey.toString(), userUsername: userUsername.toString(), userPassword: userPassword.toString(), userActive: userActive.toString(), userExpiry: userExpiry.toString()]

        } else {

            // Return the status into a list
            myList = [actionStatus: isAuthed.toString(), actionError: actionError.toString()]

        }



        // Determine the type requested and return in requested format.

        if(settingType.toString() == "JSON") {

            // Render to JSON
            render(myList as JSON)

        } else if(settingType.toString() == "XML") {

            // Render to XML
            render(myList as XML)

        } else {

            // No type specified so render as plain text
            render(myList.toString())

        }


    }











    // Index action which is used as a simple guide.

    def index() {

        def myMesg = [name: "Data API", message: "This is the root of the API - please use an API extention url."]

        render(myMesg as JSON)

    }



}

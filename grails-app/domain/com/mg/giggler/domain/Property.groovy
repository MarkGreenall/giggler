package com.mg.giggler.domain

class Property {

    String  propertyKey
    String  propertyTitle
    String  propertyComment
    Integer propertyAuthLevel
    Integer propertyActive

    static mapping = {

        propertyKey             column: 'propertyKey'
        propertyTitle           column: 'propertyTitle'
        propertyComment         column: 'propertyComment'
        propertyAuthLevel       column: 'propertyAuthLevel'
        propertyActive          column: 'propertyActive'

    }

    static constraints = {

        propertyKey(nullable: false, maxSize: 255)
        propertyTitle(nullable: false, maxSize: 255)
        propertyComment(nullable: false, maxSize: 255)
        propertyAuthLevel(nullable: false, maxSize: 11)
        propertyActive(nullable: false, maxSize: 11)

    }
}

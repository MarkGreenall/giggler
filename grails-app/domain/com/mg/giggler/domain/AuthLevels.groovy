package com.mg.giggler.domain

class AuthLevels {

    Integer authId
    String  authLevel
    String  authComment
    String  authExpiry

    static mapping = {

        authId          column: 'authId'
        authLevel       column: 'authLevel'
        authComment     column: 'authComment'
        authExpiry      column: 'authExpiry'

    }

    static constraints = {

        authId(nullable: false, maxSize: 100)
        authLevel(nullable: false, maxSize: 20)
        authComment(nullable: false, maxSize: 255)
        authExpiry(nullable: false, maxSize: 20)

    }
}
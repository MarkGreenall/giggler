package com.mg.giggler.domain

class PropertyUsers {

    String  userKey
    String  propertyKey
    String  authLevel
    String  authExpiry

    static mapping = {

        userKey         column: 'userKey'
        propertyKey     column: 'propertyKey'
        authLevel       column: 'authLevel'
        authExpiry      column: 'authExpiry'

    }

    static constraints = {

        userKey(nullable: false, maxSize: 100)
        propertyKey(nullable: false, maxSize: 255)
        authLevel(nullable: false, maxSize: 20)
        authExpiry(nullable: false, maxSize: 20)

    }
}

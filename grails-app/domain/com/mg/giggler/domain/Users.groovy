package com.mg.giggler.domain

class Users {

    String  userKey
    String  userUsername
    String  userPassword
    Integer userActive
    String  userExpiry

    static mapping = {

        userKey         column: 'userKey'
        userUsername    column: 'userUsername'
        userPassword    column: 'userPassword'
        userActive      column: 'userActive'
        userExpiry      column: 'userExpiry'

    }

    static constraints = {

        userKey(nullable: false, maxSize: 100)
        userUsername(nullable: false, maxSize: 100)
        userPassword(nullable: false, maxSize: 255)
        userActive(nullable: false, maxSize: 11)
        userExpiry(nullable: false, maxSize: 20)

    }
}

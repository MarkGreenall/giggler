
hibernate {
    cache.use_second_level_cache = false
    cache.use_query_cache = false
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}

environments {
    development {
        dataSource {
            dbCreate = "create"
            driverClassName = "com.mysql.jdbc.Driver"
            url = "jdbc:mysql://localhost/giggler"
            username = "giggler.w"
            password = 'G1gGl3R+!'
            pooled = false
        }
    }
    test {
        dataSource {
            dbCreate = "create"
            driverClassName = "com.mysql.jdbc.Driver"
            url = "jdbc:mysql://localhost/giggler"
            username = "giggler.w"
            password = 'G1gGl3R+!'
            pooled = false
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            driverClassName = "com.mysql.jdbc.Driver"
            url = "jdbc:mysql://localhost/giggler"
            username = "giggler.w"
            password = 'G1gGl3R+!'
            pooled = false
        }
    }
}
